from typing import Optional
from datetime import datetime
from pydantic import BaseModel


class ItemSchema(BaseModel):
    id: Optional[int]
    name: str
    description: str    

    class Config:
        orm_mode = True