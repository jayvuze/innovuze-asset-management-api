# Innovuze Asset Management API

REST API for management of physical assets. This is for 
Innovuze Inc. OJT program March 2022

## (Planned) Features

- Add any physical asset and categorize them
- Dispose/Remove physical asset from list
- Update information on physical asset
- Search physical assets by id, name, category

## Get Started

1. Python 3.10 or higher
2. pipenv installed
3. `pipenv install`
4. `pipenv shell`
3. `uvicorn run:api --reload`

Confirm all is working by accessing the API docs: [http://localhost:8000/docs](http://loclahost:8000/docs)

Configure your preferred text editor/IDE

- [Here is my settings.json](https://gist.github.com/killertilapia/de71971b047e3322c6f8340a352947ff) for VScode

## Tech Stack

- FastAPI
- PonyORM
- Sqlite





