from fastapi import FastAPI, Response, status
from base.models import db, Item
from base.schemas import ItemSchema
from base.config import sqlite_conf as dbcfg
from pony.orm import *

api = FastAPI()
db.bind(provider=dbcfg['provider'], filename=dbcfg['filename'], create_db=dbcfg['create_db'] )
db.generate_mapping(create_tables=True)



@api.get("/")
async def root():
    return {"message": "Hello World"}


@api.get('/items/', status_code=status.HTTP_200_OK)
async def get_all_items():
    with db_session:
        items = Item.select()
        result = [ItemSchema.from_orm(i) for i in items]
    return result

@api.get('/item/{id}/', status_code=status.HTTP_200_OK)
async def get_item(id: int, response: Response):
    try:
        with db_session:
            item = Item[id]
            result = ItemSchema.from_orm(item)

            return result
    except ObjectNotFound:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": f"Item {id} not found"}        

@api.post("/items/", status_code=status.HTTP_201_CREATED)
async def create_item(item: ItemSchema):
    with db_session:
        new_item = Item(name=item.name, description=item.description)
        commit()    # save to db

    result = ItemSchema.from_orm(new_item)
    return result

@api.put("/item/{id}", status_code=status.HTTP_200_OK)
async def update_item(id: int, item: ItemSchema, response: Response):        
    try:
        with db_session:
            updated_item = Item[id]        
            updated_item.set(name=item.name, description=item.description)
            commit()

        result = ItemSchema.from_orm(updated_item)
        return result
    except ObjectNotFound:
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": f"Item {id} not found"}

@api.delete('/item/{id}/', status_code=status.HTTP_204_NO_CONTENT)
async def remove_item(id: int, response: Response):    
    try:
        with db_session:
            item = Item[id]
            item.delete()

        return Response(status_code=status.HTTP_204_NO_CONTENT)        
    except ObjectNotFound:    
        response.status_code = status.HTTP_404_NOT_FOUND
        return {"message": f"Item {id} not found"}