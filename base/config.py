import time
from pathlib import Path

# DB connections - sqlite by default but mysql example config is provided
# mysql_conf = {'provider': 'mysql', 'host': 'localhost', 'user': 'root', 'passwd': 'root', 'db': 'dbname'}
sqlite_conf = {'provider': 'sqlite', 'filename': 'sqlite.db', 'create_db': True}

