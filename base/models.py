from datetime import datetime
from pony.orm import *

db = Database()


class Item(db.Entity):
    id = PrimaryKey(int, auto=True)
    name = Required(str)
    description = Optional(LongStr)


class Person(db.Entity):
    """
    Hello Mark
    """
    id = Primarykey(int, auto=True)
    name = Required(str)
    age = Optional(int)

